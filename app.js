import express from 'express'
import bodyParser from 'body-parser'

import userRtr from './routes/user.route'

const APP = express()
const RUTA = '/api/v1'

APP.use(bodyParser.json())
APP.use(bodyParser.urlencoded({ extended: false}))

APP.use(RUTA, userRtr)

export default APP
