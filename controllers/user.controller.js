import User from '../models/user.model'

function getUser(req, res) {
    User
        .find()
        .populate('agenda', 'name email agenda')
        .then((users) => {
            res.status(200).send(users)
        })
        .catch((err) => {
            res.status(500).send({error: err})
        })
}

function getUserById(req, res) {
    User
        .findById(req.params.userId)
        .populate({ path:'agenda', select:'name email agenda',
                populate: ({ path:'agenda', select: 'name email agenda' }) })
        .then((user) => {
            if (!user)
                return res.status(404).send({message: 'Usuario no encontrado'})
            res.status(200).send(user)
        })
        .catch((err) => {
            res.status(500).send({error: err})
        })
}

function createUser(req, res) {
    User
        .create(req.body)
        .then((userCreated) => {
            res.status(200).send(userCreated)
        })
        .catch((err) => {
            res.status(500).send({error: err})
        })
}

function updateUser(req, res) {
    User
        .findByIdAndUpdate(req.params.userId, req.body)
        .then((userUpdated) => {
            if (!userUpdated)
                return res.status(404).send({message: 'no se ha encontrado el usuario'})
            getUserById(req, res)
        })
        .catch((err) => {
            res.status(500).send({error: err})
        })
}

function removeUser(req, res) {
    User
        .findByIdAndRemove(req.params.userId)
        .then((userRemoved) => {
            res.status(200).send({message: `usuario ${userRemoved.name} fue removido`})
        })
        .catch((err) => {
            res.status(500).send({error: err})
        })
}

export { getUser, getUserById, createUser, updateUser, removeUser }
