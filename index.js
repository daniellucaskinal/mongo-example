import mongoose from 'mongoose'
import { PORT, DB } from './config'
import app from './app'

mongoose.Promise = Promise
const CONNECTION = mongoose.connect(DB, { useMongoClient: true })

CONNECTION
    .then((DB) => {
        app.listen(PORT, () => {
            console.log(`API corriendo en puerto ${PORT}`)
        })
    })
    .catch((err) => {
        console.log(err)
    })