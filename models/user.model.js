import mongoose from 'mongoose'

const Schema = mongoose.Schema
const UserSchema = Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: [String]
    },
    agenda: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    }
})

UserSchema
    .post('save', (userSaved) => {
        console.log(userSaved.name)
    })

export default mongoose.model('User', UserSchema)
