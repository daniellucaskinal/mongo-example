import express from 'express'
import { getUser, getUserById, createUser,
         updateUser, removeUser} from '../controllers/user.controller'

const ROUTER = express.Router()

ROUTER.get('/user', getUser)
ROUTER.get('/user/:userId', getUserById)
ROUTER.post('/user', createUser)
ROUTER.put('/user/:userId', updateUser)
ROUTER.delete('/user/:userId', removeUser)

export default ROUTER
